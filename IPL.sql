--IPL Data project using Sql--
--Creating Matches Table--
Create table matches(
    id integer,
    season varchar(100),
    city varchar(100),
    date DATE,
    team1 varchar(100),
    team2 varchar(100),
    toss_winner varchar(100),
    toss_decision varchar(100),
    result varchar(100),
    dl_applied int,
    winner varchar(100),
    win_by_runs int,
    win_by_wickets int,
    player_of_match varchar(100),
    venue varchar(100),
    umpire1 varchar(100),
    umpire2 varchar(100),
    umpire3 varchar(100),
    primary key (id)
);
--Creating Deliveries Table--
create table deliveries
(
    match_id integer,
    inning integer,
    batting_team varchar(100),
    bowling_team varchar(100),
    over integer,
    ball integer,
    batsman varchar(100),
    non_striker varchar(100),
    bowler varchar(100),
    is_super_over integer,
    wide_runs integer,
    bye_runs integer,
    legbye_runs integer,
    noball_runs integer,
    penalty_runs integer,
    batsman_runs integer,
    extra_runs integer,
    total_runs integer,
    player_dismissed varchar(100),
    dismissal_kind varchar(100),
    fielder varchar(100),
    primary key(match_id, inning, over, ball),
    foreign key(match_id) references matches(id)
);
--Creating Umprire Table--
CREATE TABLE umpire(
    umpire varchar(50) NOT NULL,
    country varchar(50) NOT NULL,
    PRIMARY KEY(umpire)
);
/*1. Total runs scored by team*/
SELECT batting_team,SUM(total_runs) as Total_RUN from deliveries
group by batting_team;

/*2. Top batsman for Royal Challengers Bangalore*/
SELECT batsman, SUM(batsman_runs) AS TOP_SCORE FROM deliveries
where batting_team = 'Royal Challengers Bangalore'
GROUP BY batsman
ORDER BY TOP_SCORE DESC LIMIT 10;

/*3. Foreign umpire analysis*/
SELECT country, COUNT(umpire) AS Foreign_Umpire FROM umpire
where country != ' India'
group by country;

/*4. Number of game played by team by Season*/
select team,season,sum(count) from (SELECT season, team1 as team, COUNT(id) as count from matches
group BY team1,season
UNION 
SELECT season, team2 as team, COUNT(id) as count from matches
GROUP BY team2,season) as data
group by team , season
ORDER by season;

/*5. Number of matches played per year for all the years in IPL.*/
SELECT season, COUNT(id) as Total_Matches from matches
GROUP BY season
ORDER BY season;

/*6. Number of matches won per team per year in IPL.*/
SELECT season,winner, COUNT(*) from matches
GROUP BY season, winner
ORDER BY season,winner;
/*7. Extra runs conceded per team in the year 2016 */
SELECT  bowling_team, SUM(extra_runs) AS EXTRA_RUNS from matches
INNER JOIN deliveries
ON matches.id = deliveries.match_id
where season = '2016'
group by bowling_team;

/*8.Top 10 economical bowlers in the year 2015*/
SELECT bowler, round((SUM(total_runs)/(count(*)*1.0))*6,2) AS EXTRA_RUN from  deliveries
LEFT JOIN matches
ON deliveries.match_id = matches.id
WHERE season = '2015'
GROUP BY bowler
ORDER BY EXTRA_RUN 
LIMIT 10;





